@isTest
private class StudentTriggerTest{
    /**
        * 
        * @author       Eduardo Contreras
        * @email        eduardoverd_@hotmail.com
        * @date         03/Jan/2018
        *
        * @description  This class tests the StudentTrigger 
    **/

    @isTest static void TestInsertStudent() {

        Test.startTest();//The following lines add records to the database
        Teacher__C tc=new Teacher__c(Name='John');
        insert tc;
        Classroom__c cl = new Classroom__c(name= 'Test Classroom',teachers__c=tc.id,numberOfStudents__c=6);        
        insert cl;
        Student__c st = new Student__c(Name='TestSt',classroom__c=cl.id);
        insert st;
        Student__c nst=[SELECT Name FROM Student__c WHERE ID=:st.id LIMIT 1][0];
        Classroom__c ncl=[SELECT numberOfStudents__c FROM Classroom__c WHERE ID=:cl.id LIMIT 1][0];
        Test.stopTest();


        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.debug(nst.name+' + '+ncl.numberOfStudents__c);
        System.assertEquals('TestSt (New)',nst.name);
        System.assertEquals(7,ncl.numberOfStudents__c);
    }
}