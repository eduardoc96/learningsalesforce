public class TeacherModel {
    /**
        * 
        * @author       Eduardo Contreras
        * @email        eduardoverd_@hotmail.com
        * @date         03/Jan/2018
        *
        * @description  This class handles the controller requests and interacts with the teacher table in the database
    **/

    public Teacher__c firstTeacher(){
        //Get the first teacher of the table
        Teacher__c tc=[SELECT Name,Email__c,Age__c,Birthday__c,TeacherConcat__c FROM Teacher__c LIMIT 1][0];
        return tc;
    }
    
    public Teacher__c searchTeacher(String TID){
        //Search any teacher in the table with id
        Teacher__c tc=[SELECT Name,Email__c,Age__c,Birthday__c,TeacherConcat__c FROM Teacher__c where id=:TID LIMIT 1][0];
        return  tc;
    }

    public Teacher__c searchTeacherN(String tname){
        //Search any teacher in the table by name
        Teacher__c tc=[SELECT Name,Email__c,Age__c,Birthday__c,TeacherConcat__c FROM Teacher__c where name=:tname LIMIT 1][0];
        return  tc;
    }

    public List <Teacher__c> searchTeachers(){
        //It gets all the teacher records
        List <Teacher__c> tcs=[SELECT Name,Email__c,Age__c,Birthday__c,TeacherConcat__c FROM Teacher__c];
        return tcs;
    }

    public void deleteTeacher(Teacher__c tc){
        //It deletes the teacher that it receives
        try{
            delete tc; 
        }catch(DmlException e){
            System.debug('Failure in deletion');
        }
    }

    public void updateTeacher(Teacher__c tc){
        //Update the teacher
        update tc;
    }

    public void insertTeacher(Teacher__c tc){
        //Insert a teacher
        insert tc;
    }
}