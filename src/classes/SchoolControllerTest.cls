@isTest
private class SchoolControllerTest{
    /**
        * 
        * @author       Eduardo Contreras
        * @email        eduardoverd_@hotmail.com
        * @date         03/Jan/2018
        *
        * @description  This class tests the controller, with every controller method tested by one method here
    **/

    @isTest static void updClTest(){
        Test.startTest();

        Teacher__C tc=new Teacher__c(Name='John');
        insert tc;
        Classroom__c cl = new Classroom__c(name= 'Test Classroom',teachers__c=tc.id,numberOfStudents__c=6);        
        insert cl;
        Student__c st = new Student__c(Name='TestSt',classroom__c=cl.id);
        insert st;
        
        SchoolController s=new SchoolController();

        ApexPages.currentPage().getParameters().put('tues', cl.id);
        s.updCl();    

        Test.stopTest();   

        System.assertEquals(cl.id, s.cl.id);
    }

    @isTest static void delClTest(){
        Test.startTest();

        insertData();
        SchoolController s=new SchoolController();
        Classroom__c cl=[SELECT numberOfStudents__C FROM Classroom__c WHERE name=:'Test Classroom1' LIMIT 1][0];
        ApexPages.currentPage().getParameters().put('tues', cl.id);
        s.delCl();    
        Classroom__c deleted = [SELECT Id, IsDeleted FROM Classroom__c WHERE Id = :cl.Id ALL ROWS];
        Test.stopTest();   

        System.assertEquals(true,deleted.isDeleted);
    }

    @isTest static void updTcTest(){
        Test.startTest();

        insertData();       
        SchoolController s=new SchoolController();
        Teacher__c tc=[SELECT NAME FROM Teacher__c WHERE name='John' LIMIT 1][0];
        ApexPages.currentPage().getParameters().put('tues', tc.id);
        s.updTc();    

        Test.stopTest();   

        System.assertEquals(tc.id, s.tc.id);
    }

    @isTest static void delTcTest(){
        Test.startTest();

        insertData();
        SchoolController s=new SchoolController();
        Teacher__c tc=[SELECT Name FROM Teacher__c WHERE name=:'John' LIMIT 1][0];
        ApexPages.currentPage().getParameters().put('tues', tc.id);
        s.delTc();    
        
        Teacher__c deleted=[SELECT Name,isDeleted FROM Teacher__c WHERE id = :tc.id ALL ROWS];
        Test.stopTest();   

        System.assertEquals(deleted.isDeleted, true);
    }

    @isTest static void updStTest(){
        Test.startTest();

        insertData();       
        SchoolController s=new SchoolController();
        Student__c st=[SELECT NAME FROM Student__c WHERE name='TestSt2' LIMIT 1][0];
        ApexPages.currentPage().getParameters().put('tues', st.id);
        s.updSt();    

        Test.stopTest();   

        System.assertEquals(st.id, s.st.id);
    }

    @isTest static void delStTest(){
        Test.startTest();

        insertData();
        SchoolController s=new SchoolController();
        Student__c st=[SELECT Name FROM Student__c WHERE name=:'TestSt1' LIMIT 1][0];
        ApexPages.currentPage().getParameters().put('tues', st.id); 
        s.delSt();    
        
        Student__c deleted=[SELECT Name,isDeleted FROM Student__c WHERE id = :st.id ALL ROWS];
        Test.stopTest();   

        System.assertEquals(deleted.isDeleted, true);
    }

    @isTest static void addStudentTest(){
        Test.startTest();
        insertData();
        SchoolController sc=new SchoolController();
        Classroom__C cl=[SELECT Name FROM Classroom__c WHERE Name='Test Classroom2'];
        Student__c st = new Student__c(name='TestSt5',classroom__c=cl.id);
        sc.newStudent=st;
        sc.addStudent();
        Student__c stest=[SELECT Name FROM Student__c WHERE Name='TestSt5'];
        Test.stopTest();

        System.assertEquals(stest.name,st.name);
    }

    @isTest static void addTeacherTest(){
        Test.startTest();
        insertData();
        SchoolController sc=new SchoolController();
        Teacher__c tc = new Teacher__c(name='TeacherTest');
        sc.newTeacher=tc;
        sc.addTeacher();
        Teacher__c ttest=[SELECT Name FROM Teacher__c WHERE Name='TeacherTest'];
        Test.stopTest();

        System.assertEquals(ttest.name,tc.name);
    }

    @isTest static void addClassroomTest(){
        Test.startTest();
        insertData();
        SchoolController sc=new SchoolController();
        Teacher__c tc=[SELECT Name FROM Teacher__c WHERE Name='John'];
        Classroom__C cl=new Classroom__c(Name='Room 101',teachers__c=tc.id);
        sc.newClassroom=cl;
        sc.addClassroom();
        Classroom__c ctest=[SELECT Name FROM Classroom__c WHERE Name='Room 101'];
        Test.stopTest();

        System.assertEquals(ctest.name,cl.name);
    }

    @isTest static void searchTeacherTest(){
        Test.startTest();
        insertData();
        SchoolController sc=new SchoolController();
        sc.searcht='John';
        sc.searchTeacher();
        Test.stopTest();

        System.assertEquals('John', sc.tc.name);
    }

    @isTest static void searchClassroomTest(){
        Test.startTest();
        insertData();
        SchoolController sc=new SchoolController();
        sc.searchc='Test Classroom2';
        sc.searchClassroom();
        Test.stopTest();

        System.assertEquals('Test Classroom2', sc.cl.name);
    }

    @isTest static void searchStudentTest(){
        Test.startTest();
        insertData();
        SchoolController sc=new SchoolController();
        sc.searchs='TestSt1';
        sc.searchStudent();
        Test.stopTest();

        System.assertEquals('TestSt1', sc.st.name);
    }
    
    @isTest static void saveRecordTeacherTest(){
        insertData(); 
        Test.startTest();
        Teacher__c tc=[SELECT Name FROM Teacher__c WHERE Name='John'];    
        SchoolController t=new SchoolController();    
        tc.name='John Modified';
        t.tc=tc;
        t.saveRecordTeacher();
        Teacher__c ntc=null;
        try{
            ntc=[SELECT Name FROM Teacher__c WHERE Name='John'];
        }catch(QueryException qe){}
        
        Teacher__c nntc=[SELECT Name FROM Teacher__c WHERE Name='John Modified'];  
        Test.stopTest();
        System.assertEquals(ntc, null);
        System.assertEquals(nntc.Name, 'John Modified');
    }

    @isTest static void saveRecordStudentTest(){
        insertData();
        Test.startTest();
        Student__c st=[SELECT Name FROM Student__c WHERE Name='TestSt1'];    
        SchoolController s=new SchoolController();   
        st.name='TestSt1 Modified';
        s.st=st;
        s.saveRecordStudent();
        Student__c ntc=null;
        try{
            ntc=[SELECT Name FROM Student__c WHERE Name='TestSt1'];
        }catch(QueryException qe){}
        
        Student__c nntc=[SELECT Name FROM Student__c WHERE Name='TestSt1 Modified'];  
        Test.stopTest();
        System.assertEquals(ntc, null);
        System.assertEquals(nntc.Name, 'TestSt1 Modified');
    }

    @isTest static void saveRecordClassroomTest(){
        insertData();
        Test.startTest();
        Classroom__c cl=[SELECT Name FROM Classroom__c WHERE Name='Test Classroom1'];
        SchoolController s=new SchoolController();        
        cl.name='Test Classroom1 Modified';
        s.cl=cl;
        s.saveRecordClassroom();
        update cl;
        Classroom__c ncl=null;
        try{
            ncl=[SELECT Name FROM Classroom__c WHERE Name='Test Classroom1'];
        }catch(QueryException qe){}
        
        Classroom__c nncl=[SELECT Name FROM Classroom__c WHERE Name='Test Classroom1 Modified'];  
        Test.stopTest();
        System.assertEquals(ncl, null);
        System.assertEquals(nncl.Name, 'Test Classroom1 Modified');
    }

    @isTest static void deleteTeacherTest(){
        insertData(); 
        Test.startTest();
        Teacher__c tc=[SELECT Name FROM Teacher__c WHERE Name='John'];    
        SchoolController t=new SchoolController();    
        t.deleteTeacher();
        t.tc=tc;
        t.deleteTeacher();
        Teacher__c ntc=null;
        try{
            ntc=[SELECT Name FROM Teacher__c WHERE Name='John'];
        }catch(QueryException qe){}
        Test.stopTest();
        System.assertEquals(ntc, null);
    }

    @isTest static void deleteStudentTest(){
        insertData();
        Test.startTest();
        Student__c st=[SELECT Name FROM Student__c WHERE Name='TestSt1'];    
        SchoolController s=new SchoolController(); 
        s.deleteStudent(); 
        s.st=st;
        s.deleteStudent();
        Student__c nst=null;
        try{
            nst=[SELECT Name FROM Student__c WHERE Name='TestSt1'];
        }catch(QueryException qe){}
        Test.stopTest();
        System.assertEquals(null, nst);
    }

    @isTest static void deleteClassroomTest(){
        insertData();
        Test.startTest();
        Classroom__c cl=[SELECT Name FROM Classroom__c WHERE Name='Test Classroom1'];
        SchoolController s=new SchoolController();
         s.deleteClassroom();
        s.cl=cl;
        s.deleteClassroom();
        Classroom__c ncl=null;
        try{
            ncl=[SELECT Name FROM Classroom__c WHERE Name='Test Classroom1'];
        }catch(QueryException qe){}
        Test.stopTest();
        System.assertEquals(ncl, null);
    }
    
    public static void insertData(){
        Teacher__C tc=new Teacher__c(Name='John');
        insert tc;
        tc=new Teacher__c(Name='Joana');
        insert tc;
        Classroom__c cl = new Classroom__c(name= 'Test Classroom1',teachers__c=tc.id,numberOfStudents__c=2);
        insert cl; 
        cl = new Classroom__c(name= 'Test Classroom2',teachers__c=tc.id,numberOfStudents__c=2);        
        insert cl;
        Student__c st = new Student__c(name='TestSt1',classroom__c=cl.id);
        insert st;
        st = new Student__c(name='TestSt2',classroom__c=cl.id);
        insert st;
    }
}