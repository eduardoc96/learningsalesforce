global class batchExample implements Database.Batchable<sObject> {
    /**
        * Webkul Software.
        *
        * @category  Webkul
        * @author    Webkul
        * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
        * @license   https://store.webkul.com/license.html
    */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
          
        String query = 'SELECT Id,Name,NumberOfStudents__c FROM Classroom__c';
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<Classroom__c> clList) {
        
        // process each batch of records
         
        for(Classroom__c cl : clList)
        {        
            // Ask if the classroom has 0 students

            if(cl.NumberOfStudents__c==0){
                //Si tiene 0 lo borra
                try{
                    delete cl;   
                }catch(DmlException e){
                    System.debug('Deletion Failure '+e);
                }
                
            }
        }
    }   
     
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
  }
}