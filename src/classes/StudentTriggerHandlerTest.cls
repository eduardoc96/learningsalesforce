@isTest
private class StudentTriggerHandlerTest{
    /**
        * 
        * @author       Eduardo Contreras
        * @email        eduardoverd_@hotmail.com
        * @date         03/Jan/2018
        *
        * @description  This class tests the StudentTriggerHandler
    **/

    @isTest static void generalTest(){
        //The next lines add data
        Test.startTest();
        Teacher__C tc=new Teacher__c(Name='John');//Add a new teacher
        insert tc;
        Classroom__c cl = new Classroom__c(name= 'Test Classroom',teachers__c=tc.id,numberOfStudents__c=5);//Add a new classroom with 5 students      
        insert cl;
        Student__c st = new Student__c(Name='TestSt',classroom__c=cl.id);//Add a new student to the classroom that was just added
        insert st;
        StudentTriggerHandler s=new StudentTriggerHandler(); //Create an instance of studentTriggerHandler
        Classroom__c testcl1=s.getClassroomNOStudents(st);   //Get the classroom number of students

        Student__c nst=[SELECT Name FROM Student__c WHERE ID=:st.id LIMIT 1][0];//Query the student cl, because the cl
        //object is not affected by the NewStudentTrigger so we cant know if the trigger actually works, thats why the student
        //is again queried
        Classroom__c ncl=[SELECT NumberOfStudents__c FROM Classroom__c WHERE ID=:cl.id LIMIT 1][0];
        

    	ncl=s.increaseNOStudents(ncl);//This line increases the number of students of a classroom by 1 

        Test.stopTest();
        System.assertEquals(6, testcl1.numberOfStudents__c);//This two lines do a comparison of the test 
        System.assertEquals(7, ncl.numberOfStudents__c);
        
    }

}