public class SchoolController {
    /**
        * 
        * @author       Eduardo Contreras
        * @email        eduardoverd_@hotmail.com
        * @date         03/Jan/2018
        *
        * @description  This class is the main controller of this proyect, it handles the requests from SchoolRegistry visualforce
        * page and takes action as required by every method for all three custom objects, Teacher, Classroom and Student.
    **/

    public String searchs {get;set;}//The three search strings are intended to contain the name to seach for a record, like a classroom, student or
    public String searchc {get;set;}//a classoom, they are te content of the search boxes
    public String searcht {get;set;}

    public Student__c st {get;set;}//This three student objets are intended to save several records of Students, they are a way to link the data in
    public List <Student__c> sts {get;set;}//the visualforce page with the salesforce database
    public Student__c newStudent {get;set;}

    public Teacher__c tc {get;set;}//This three classroom objects are intended to save several records of teacher, link records of theachers in the 
    public List <Teacher__c> tcs {get;set;}//visualforce page 
    public Teacher__c newTeacher {get;set;}

    public Classroom__c cl {get;set;}//This three classroom objects are intended to save several records of  classroom, and link the fields in the
    public List <Classroom__c> cls {get;set;}//visualforce pages with the database
    public Classroom__c newClassroom {get;set;}

    public StudentModel s;//The three model objects are intended to create an instance of their respective class in this class
    public TeacherModel t;
    public ClassroomModel c; 
    
    public SchoolController(){
        //This constructor sets all the objects required by the SchoolRegistry to work
        s=new StudentModel();//
        sts=s.searchStudents();
        st=s.firstStudent();  
        newStudent=s.firstStudent();
        t=new TeacherModel();
        tcs=t.searchTeachers();
        tc=t.firstTeacher(); 
        newTeacher=t.firstTeacher();
        c=new ClassroomModel();
        cls=c.searchClassrooms();
        cl=c.firstClassroom();
        newClassroom=c.firstClassroom();


        Map<Id, Map<String,Student__c>> classrooms = new Map<Id, Map<String,Student__c>>();

        List<Classroom__c> cls=[SELECT id,name FROM Classroom__c]; 
        List<Student__c> sts=[SELECT id,name,Classroom__r.id,Classroom__r.name FROM Student__C ORDER BY Name asc];
        sts.sort();

        for(Classroom__c cl : cls){

            
            Map<String,Student__c> msts = new Map<String,Student__c>();

            for(Student__c st : sts){
                if(st.Classroom__r.id==cl.id){//If the student classroom id is equal to this classroom id
                    msts.put(st.name,st);//msts Will have all the students with this classroom ID
                }
            }
        
            classrooms.put(cl.id,msts); //Add the map of students of this classroom to the classrooms map
        }

        System.debug(classrooms);
    }

    public void UpdateTables(){
        //This method updates the three objects whose data creates the tables, sts(Students), tcs(Teachers) and cls(Clients)
        s=new StudentModel();
        sts=s.searchStudents();
        t=new TeacherModel();
        tcs=t.searchTeachers();
        c=new ClassroomModel();
        cls=c.searchClassrooms();
    }

    public void updCl(){
        //This method is linked with the update button in the classroom table, it gets the 'tues' parameter that saves the
        //classroom id of that row and then puts that data in the cl objects that is linked to the fields above so the user
        //can edit them
        String currentRow;
        currentRow = ApexPages.currentPage().getParameters().get('tues');
        c=new ClassroomModel();
        cl=c.searchClassroom(currentRow);
        updateTables();
    }

    public void delCl(){
        //This method is linked with the delete button in the classroom table, it gets the 'tues' parameter that saves the
        //classroom id of that row and then deletes that classroom
        String currentRow;
        currentRow = ApexPages.currentPage().getParameters().get('tues');
        c=new ClassroomModel();
        cl=c.searchClassroom(currentRow);
        c.deleteClassroom(cl);
        cl=c.firstClassroom();
        updateTables();
    }

    public void updTc(){
        //This method is linked with the update button in the teacher table, it gets the 'tues' parameter that saves the
        //teacher id of that row and then puts that data in the tc objects that is linked to the fields above so the user
        //can edit them
        String currentRow;
        currentRow = ApexPages.currentPage().getParameters().get('tues');
        t=new TeacherModel();
        tc=t.searchTeacher(currentRow);
        updateTables();
    }

    public void delTc(){
        //This method is linked with the delete button in the teacher table, it gets the 'tues' parameter that saves the
        //teacher id of that row and then deletes that teacher
        String currentRow;
        currentRow = ApexPages.currentPage().getParameters().get('tues');
        t=new TeacherModel();
        tc=t.searchTeacher(currentRow);
        t.deleteTeacher(tc);
        tc=t.firstTeacher();
        updateTables();
    }

    public void updSt(){
        //This method is linked with the update button in the student table, it gets the 'tues' parameter that saves the
        //student id of that row and then puts that data in the cl objects that is linked to the fields above so the user
        //can edit them
        String currentRow;
        currentRow = ApexPages.currentPage().getParameters().get('tues');
        s=new StudentModel();
        st=s.searchStudent(currentRow);
        updateTables();
    }

    public void delSt(){
        //This method is linked with the delete button in the student table, it gets the 'tues' parameter that saves the
        //student id of that row and then deletes that student
        String currentRow;
        currentRow = ApexPages.currentPage().getParameters().get('tues');
        s=new StudentModel();
        st=s.searchStudent(currentRow);
        s.deleteStudent(st);
        System.debug('hola');
        updateTables();
    }
    
    public void addStudent(){
        //This method is linked with the insert button, it gets the information in the NewStudent object, nullifies the NewStudent
        //id and insert that student
        s=new StudentModel();
        newStudent.id=null;
        s.insertStudent(newStudent);
        st=newStudent;
        newStudent=s.firstStudent();
        updateTables();
    }
    
    public void addTeacher(){
        //This method is linked with the insert button, it gets the information in the NewTeacher object, nullifies the NewTeacher
        //id and insert that teacher
        t=new TeacherModel();
        newTeacher.id=null;
        t.insertTeacher(newTeacher);
        tc=newTeacher;
        newTeacher=t.firstTeacher();
        updateTables();
    }
    
    public void addClassroom(){
        //This method is linked with the insert button, it gets the information in the NewClassroom object, nullifies the
        //newClassroom id and insert that classroom
        c=new ClassroomModel();
        newClassroom.id = null;
        c.insertClassroom(newClassroom);
        cl=newClassroom;
        newClassroom=c.firstClassroom();
        updateTables();
    }   
    
    public void searchStudent(){
        //This method search the student whose name is in the search by name text field
        s=new StudentModel();
        st=s.searchStudentN(searchs);
    }
    
    public void searchTeacher(){
        //This method search the teacher whose name is in the search by name text field
        t=new TeacherModel();
        tc=t.searchTeacherN(searcht); 
        updateTables();
    }
    
    public void searchClassroom(){
        //This method search the student whose name is in the search by name text field
        c=new ClassroomModel();
        cl=c.searchClassroomN(searchc);
        updateTables();
    }
    
    public void saveRecordStudent(){
        //This method saves the student whose data is in the edit student fields
        s=new StudentModel();
        s.updateStudent(st);
        updateTables();
    }
    
    public void saveRecordTeacher(){
        //This method saves the teacher whose data is in the edit teacher fields
        t=new TeacherModel();
        t.updateTeacher(tc);
        updateTables();
    }
    
    public void saveRecordClassroom(){
        //This method saves the classroom whose data is in the edit classroom fields
        c=new ClassroomModel();
        c.updateClassroom(cl);
        updateTables();
    }
    
    public void deleteStudent(){
        //This method deletes the student whose data is in the edit student fields
        s=new StudentModel();
        s.deleteStudent(st);
        st=s.firstStudent();
        updateTables();
    }
    
    public void deleteClassroom(){
        //This method deletes the classroom whose data is in the edit classroom fields
        c=new ClassroomModel();
        c.deleteClassroom(cl);
        cl=c.firstClassroom();
        updateTables();
    }
    
    public void deleteTeacher(){
        //This method deletes the teacher whose data is in the edit teacher fields
        t=new TeacherModel();
        t.deleteTeacher(tc);
        tc=t.firstTeacher();
        updateTables();
    }
    
    
}