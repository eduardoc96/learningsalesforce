public class ClassroomModel {
    /**
        * 
        * @author       Eduardo Contreras
        * @email        eduardoverd_@hotmail.com
        * @date         03/Jan/2018
        *
        * @description  This class handles the controller requests and interacts with the classroom table in the database
    **/

    public Classroom__c firstClassroom(){
        //Get the first classroom of the table
        Classroom__c cl=[SELECT Name,Department__c,Capacity__c,openingDate__c,Teachers__c,ClassroomConcat__c,NumberOfStudents__c FROM Classroom__c LIMIT 1][0];
        return cl;
    }

    public Classroom__c searchClassroom(String CID){
        //Search any classroom in the table with id
        Classroom__c cl=[SELECT Name,Department__c,Capacity__c,OpeningDate__c,Teachers__c,ClassroomConcat__c,NumberOfStudents__c FROM Classroom__c where ID=:CID LIMIT 1][0];
        return cl;
    }

    public Classroom__c searchClassroomN(String cname){
        //Search any classroom in the table by name
        Classroom__c cl=[SELECT Name,Department__c,Capacity__c,OpeningDate__c,Teachers__c,ClassroomConcat__c,NumberOfStudents__c FROM Classroom__c where name=:cname LIMIT 1][0];
        return cl;
    }

    public List<Classroom__c> searchClassrooms(){
        //It gets all the classroom records
        List <Classroom__c> cls=[SELECT Name,Department__c,Capacity__c,OpeningDate__c,Teachers__c,ClassroomConcat__c,NumberOfStudents__c FROM Classroom__c];
        return cls;
    }

    public void deleteClassroom(Classroom__c cl){
        //It deletes the classroom that it receives
        try{
        	delete cl;
        }catch(DMLException e){
            System.debug('Error en borrado');
        }  
    }

    public void updateClassroom(Classroom__c cl){
        //Update the classroom
        update cl;
    }

    public void insertClassroom(Classroom__c cl){
        //Insert a classroom
        insert cl;
    }
}