public class StudentTriggerHandler {
    /**
        * 
        * @author       Eduardo Contreras
        * @email        eduardoverd_@hotmail.com
        * @date         03/Jan/2018
        *
        * @description  This class handles the database operation of the NewStudentTrigger
    **/

    public Classroom__c getClassroomNOStudents(Student__C st){
        //This method gets the number of students of the classroom a student is related to
        Classroom__c cl=[SELECT NumberOfStudents__c FROM Classroom__c WHERE ID=:st.Classroom__c LIMIT 1][0];
        return cl;
    }

    public Classroom__c increaseNOStudents(Classroom__c cl){
        //This method increases the number of students of a classroom in 1 student
        cl.NumberOfStudents__c++;
        return cl;
    }

    public void updateClassroom(Classroom__c cl){
        //This method updates the classroom it receives
        ClassroomModel cm=new ClassroomModel();
        cm.updateClassroom(cl);
    }

    public void verifyNOStudents(Classroom__c cl,Student__c st){
        //This method asks if a classroom has 5 or more students and in that case appends the string (new) at the end
        if(cl.NumberOfStudents__c>=5){
            st.Name=st.Name+(' (New)');
        }
    }
}