global class ClassroomBatchScheduler implements Schedulable {
    /**
        * 
        * @author       Eduardo Contreras
        * @email        eduardoverd_@hotmail.com
        * @date         03/Jan/2018
        * 
        * @description  This class executes every week and can be configured as requires, it executes the EmptyClassroomBatch
        * class.
    **/

    // Execute at regular intervals
    global void execute(SchedulableContext ctx){
      //It creates an instance of DeleteEmptyClassroomBatch and executes it
      DeleteEmptyClassroomBatch batch = new DeleteEmptyClassroomBatch();
      Database.executebatch(batch); 
    }
}