@isTest
private class DeleteEmptyClassroomBatchTest{
    /**
        * 
        * @author       Eduardo Contreras
        * @email        eduardoverd_@hotmail.com
        * @date         03/Jan/2018
        *
        * @description  This class is a test for 
    **/
    @isTest static void main(){
        Test.startTest();
        insertData();
        DeleteEmptyClassroomBatch batch = new DeleteEmptyClassroomBatch();
        Database.executebatch(batch);
        integer cont=0;
        Classroom__c clList;
        List <Classroom__c> cll=new List<Classroom__c>();
        for(Classroom__c cl : cll)
        {        
            // Ask if the classroom has 0 students
            if(cl.NumberOfStudents__c==0){
                //If it has 0 then it deletes it
                cont++;
            }
        }
        Test.stopTest();

        System.assertEquals(0,cont);
    }
    
    public static void insertData(){
        Teacher__C tc=new Teacher__c(Name='John');
        insert tc;
        tc=new Teacher__c(Name='Joana');
        insert tc;
        Classroom__c cl = new Classroom__c(name= 'Test Classroom1',teachers__c=tc.id,numberOfStudents__c=0);
        insert cl; 
        cl = new Classroom__c(name= 'Test Classroom2',teachers__c=tc.id,numberOfStudents__c=0);        
        insert cl;
        Student__c st = new Student__c(name='TestSt1',classroom__c=cl.id);
        insert st;
    }
}