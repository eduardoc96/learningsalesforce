public class StudentModel {
    /**
        * 
        * @author       Eduardo Contreras
        * @email        eduardoverd_@hotmail.com
        * @date         03/Jan/2018
        *
        * @description  This class handles the controller requests and interacts with the Student table in the database
    **/

    public Student__c firstStudent(){
        //Get the first student of the table
        Student__c st=[SELECT Name,LastName__c,Age__c,group__c,Birthday__c,Classroom__c,StudentConcat__c FROM Student__c LIMIT 1][0];
        return st;
    }

    public Student__c searchStudent(String SID){
        //Search any student in the table with id
        Student__c st=[SELECT Name,LastName__c,Age__c,group__c,Birthday__c,Classroom__c,StudentConcat__c FROM Student__c where ID=:SID LIMIT 1][0];
        return st;
    }

    public Student__c searchStudentn(String sname){
        //Search any student in the table by name
        Student__c st=[SELECT Name,LastName__c,Age__c,group__c,Birthday__c,Classroom__c,StudentConcat__c FROM Student__c where name=:sname LIMIT 1][0];
        return st;
    }
    
    public List <Student__c> searchStudents(){
        //It gets all the student records
        List <Student__c> sts=[SELECT Name,LastName__c,Age__c,group__c,Birthday__c,Classroom__c,StudentConcat__c FROM Student__c];
        return sts;
    }

    public void deleteStudent(Student__c st){
        //It deletes the student that it receives
        try{
            delete st;
        }catch(DmlException e){
            System.debug('Failure in deletion '+e);
        }
    }
    
    public void updateStudent(Student__c st){
        //Update the student
        update st;
    }

    public void insertStudent(Student__c st){
        //Insert a student
        insert st;
    }
}