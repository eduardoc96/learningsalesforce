trigger NewStudentTrigger on Student__c (after insert, before insert, before update, after update, before delete, after delete) {
    /**
        * 
        * @author       Eduardo Contreras
        * @email        eduardoverd_@hotmail.com
        * @date         03/Jan/2018
        *
        * @description  This trigger handles the before insert and after insert, helped nt the StudentTriggerHandler
    **/
    StudentTriggerHandler s=new StudentTriggerHandler();

    if(Trigger.isInsert){

        if(Trigger.isBefore){
            //This code block gets the inserted record, the number of students of the classroom
            System.debug('Before must add new to name');
            Student__c st=Trigger.new[0];
            Classroom__c cl=s.getClassroomNOStudents(st);
            s.verifyNOStudents(cl,st);//This  methos verifies if the student classroom has 5 or more students and in that case adds
            //the string (new) at the end of the student name
            
        }else{

            if(Trigger.isAfter){
                //This code block gets the inserted record, the number of students of the classroom
                System.debug('After must increase number of students');
                Student__c st=Trigger.new[0];
                Classroom__c cl=s.getClassroomNOStudents(st);//This line gets the classroom number of students
                System.debug(cl.numberOfStudents__c);
                cl=s.increaseNOStudents(cl);//This line increases the number of student
                System.debug(cl.numberOfStudents__c);
                s.updateClassroom(cl);

            }
        }
    }else{
        
    }
}